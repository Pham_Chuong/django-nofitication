from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'example.views.home', name='home'),
    url(r'^alert/$', 'example.views.alert', name='alert'),
    url(r'^accounts/login/$','django.contrib.auth.views.login',name='login'),
    url(r'^admin/', include(admin.site.urls)),
)
